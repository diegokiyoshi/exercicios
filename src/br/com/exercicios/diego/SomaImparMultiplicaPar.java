package br.com.exercicios.diego;

/*
 * Escreva um programa que imprima na tela a soma dos n�meros �mpares entre 0 e
 * 30 e a multiplica��o dos n�meros pares entre 0 e 30.
 */

public class SomaImparMultiplicaPar {
	
	Integer soma = 0;
	long multiplica = 0;
	
	public int somarImpar() {
		
		for(int i = 0; i <= 30; i ++) {
			if(i % 2 != 0) {
				soma += i;
			} 
		}
		return soma;

	}
	
	public long multiplicarPar() {
		
		for(int i = 0; i < 30; i ++) {
			if(i % 2 == 0) {
				if(i > 2) {
					multiplica *= i;
				} else {
					multiplica = i;
				}
				
			}
		}
		
		return multiplica;
	}
}
