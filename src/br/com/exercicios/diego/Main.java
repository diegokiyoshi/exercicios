package br.com.exercicios.diego;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		/*
		 * 1. Crie uma classe java MaiorNumero que contenha um m�todo que receba dois
		 * n�meros inteiros e imprima o maior entre eles 
		 */
		
//		MaiorNumero maiorNumero = new MaiorNumero();
//		System.out.println(maiorNumero.maiorNumero(123, 444));
		
/*****************************************************************************************************************************************/		
		
		/*
		 * Crie uma classe java NumeroDecrescente que contenha um m�todo que receba um
		 * n�mero inteiro e imprima, em ordem decrescente, o valor do n�mero at� 0.
		 */
				
//		NumeroDecrescente numerosDecrescente = new NumeroDecrescente();
//		numerosDecrescente.definirNumeroDecrescente(10);

/*****************************************************************************************************************************************/		
		
		/*
		 * Escreva um programa que imprima na tela a soma dos n�meros �mpares entre 0 e
		 * 30 e a multiplica��o dos n�meros pares entre 0 e 30.
		 */
		
//		SomaImparMultiplicaPar somaImpMultiplicaPar = new SomaImparMultiplicaPar();
//		System.out.println(somaImpMultiplicaPar.somarImpar());
//		System.out.println(somaImpMultiplicaPar.multiplicarPar());
		
/******************************************************************************************************************************************/		
		
		/*
		 * Crie uma classe java TrocaNumero que contenha um m�todo que receba dois
		 * n�meros NumA e NumB, nessa ordem, e imprima em ordem inversa, isto �, se os
		 * dados lidos forem NumA = 5 e NumB = 9, por exemplo, devem ser impressos na
		 * ordem NumA = 9 e NumB = 5.
		 */
		
//		TrocaNumero trocaNumero = new TrocaNumero();
//		trocaNumero.trocarNumero(14, 65);

/*******************************************************************************************************************************************/		
		
		/*
		 * 5. Crie uma classe java ComparaNumero que contenha um m�todo que receba dois
		 * n�meros e indique se s�o iguais ou se s�o diferentes. Mostre o maior e o
		 * menor (nesta sequ�ncia).
		 */
		
//		ComparaNumero comparaNumero = new ComparaNumero();
//		comparaNumero.compararNumeros(10, 13);
		
/********************************************************************************************************************************************/		
		
		/*
		 * 6. Crie uma classe MediaAluno que contenha um atributo do tipo vetor de
		 * inteiros com o nome de notas. Essa classe deve ter um m�todo para adicionar
		 * as notas nesse vetor (os valores que podem ser adicionados no vetor s�o os
		 * inteiros entre 0 e 100, caso contr�rio imprime uma mensagem de erro e n�o
		 * adiciona) e outro m�todo que calcule a m�dia de um aluno e imprima essa m�dia
		 */
		
//		MediaAluno notaAluno = new MediaAluno();
//		notaAluno.adicionarNota(10);
//		notaAluno.adicionarNota(20);
//		notaAluno.adicionarNota(80);
//		notaAluno.adicionarNota(30);
//		notaAluno.adicionarNota(50);
//		notaAluno.adicionarNota(20);
//		notaAluno.adicionarNota(70);
//		notaAluno.adicionarNota(10);
//		notaAluno.adicionarNota(120);
//		
//		for (Integer notas : notaAluno.listaNotas) {
//			System.out.println(notas);
//		}
//		
//		notaAluno.imprimirMediaDasNotas();
		
/*****************************************************************************************************************************************/
		
		/*
		 * Crie uma classe Contato que possui dois atributos: nome e email do tipo
		 * String. Crie outra classe, chamada Agenda, que possui um atributo contatos do
		 * tipo vetor de Contato. A classe Agenda deve conter um m�todo para adicionar
		 * um novo contato em uma posi��o vazia do vetor, outro m�todo para buscar um
		 * contato (retorna uma inst�ncia de Contato) atrav�s do nome e, por fim, um
		 * m�todo para excluir um contato atrav�s do nome.
		 */
		
		Contato contato1 = new Contato();
		Contato contato2 = new Contato();
		Contato contato3 = new Contato();
		
		Agenda agenda = new Agenda();
		
		contato1.setNome("Diego");
		contato1.setEmail("di.kiyoshi@gmail.com");
		
		contato2.setNome("Osmar");
		contato2.setEmail("osmar@gmail.com");

		contato3.setNome("Allan");
		contato3.setEmail("allan@gmail.com");

		agenda.adicionarContato(contato1);
		agenda.adicionarContato(contato2);
		agenda.adicionarContato(contato3);
		
		//agenda.excluirContato(contato1);
		
		System.out.println(agenda.pegarContato(contato2));
		
		Contato instanciaRetornada = agenda.buscarContato("Osmar");
		System.out.println("O nome � igual: " + instanciaRetornada.getNome());
		for (Contato contato : agenda.listaContatos) {
			System.out.println("Nome: " + contato.getNome() + " Email: " + contato.getEmail());
		}
		
	}
}

	