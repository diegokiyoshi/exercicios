package br.com.exercicios.diego;

/*
 * 6. Crie uma classe MediaAluno que contenha um atributo do tipo vetor de
 * inteiros com o nome de notas. Essa classe deve ter um m�todo para adicionar
 * as notas nesse vetor (os valores que podem ser adicionados no vetor s�o os
 * inteiros entre 0 e 100, caso contr�rio imprime uma mensagem de erro e n�o
 * adiciona) e outro m�todo que calcule a m�dia de um aluno e imprima essa m�dia
 */

import java.util.ArrayList;

public class MediaAluno {

	ArrayList<Integer> listaNotas = new ArrayList<Integer>();

	public void adicionarNota(Integer nota) {
		if (nota < 0 || nota > 100) {
			System.out.println("Nota inv�lida");
		} else {
			listaNotas.add(nota);

		}
	}

	public void imprimirMediaDasNotas() {
		Integer total = 0;
		
		for (Integer integer : listaNotas) {
			total += integer;
		}
		System.out.println("A m�dia das notas �: " + total/listaNotas.size());
	}
	
	
}
