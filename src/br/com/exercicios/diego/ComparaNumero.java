package br.com.exercicios.diego;

/*
 * 5. Crie uma classe java ComparaNumero que contenha um m�todo que receba dois
 * n�meros e indique se s�o iguais ou se s�o diferentes. Mostre o maior e o
 * menor (nesta sequ�ncia).
 */

public class ComparaNumero {

	Integer maior;
	Integer menor;

	public void compararNumeros(Integer numA, Integer numB) {

		if (numA > numB) {
			maior = numA;
			menor = numB;
			
		} else  {
			maior = numB;
			menor = numA;	
		} 
		
		if(maior == menor) {
			System.out.println("Os n�meros s�o iguais ");
		} else {
			System.out.println("O maior � igual a " + maior + "\n" + "E o menor numero �: " + menor);
		}
	}
	
}
