package br.com.exercicios.diego;


import java.util.ArrayList;
import java.util.List;

public class Agenda {
 
	public List<Contato> listaContatos = new ArrayList<Contato>();
	
	public void adicionarContato(Contato contato) {
		
		listaContatos.add(contato);
		
	}
	
	public Contato buscarContato(String nome) {
		Contato contatoEncontrado = null;
		for (Contato contato : listaContatos) {
			if(nome.equalsIgnoreCase(contato.getNome())) {	
				contatoEncontrado = contato;
			}	 
		}
		
		return contatoEncontrado;
		
		
	}
	
	public Contato pegarContato(Contato contato) {
		return contato;
	}
	
	public void excluirContato(Contato contato) {
		listaContatos.remove(contato);
	}

	public List<Contato> getListaContatos() {
		return listaContatos;
	}

	public void setListaContatos(List<Contato> listaContatos) {
		this.listaContatos = listaContatos;
	}
}
