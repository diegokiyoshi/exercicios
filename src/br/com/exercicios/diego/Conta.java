package br.com.exercicios.diego;

public class Conta {
	public static void main(String[] args) {
		for(int i = 1; i <= 10; i++) {
			System.out.print("Tabuada do " + i + ":  ");
			for(int j = 0; j <= 10; j ++) {
				System.out.print(i * j);
				System.out.print(" ");
			}
			System.out.println( );  
		}
	}
}
