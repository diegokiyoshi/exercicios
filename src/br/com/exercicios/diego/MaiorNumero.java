package br.com.exercicios.diego;
/*
 * 1. Crie uma classe java MaiorNumero que contenha um m�todo que receba dois
 * n�meros inteiros e imprima o maior entre eles 
 */
public class MaiorNumero {
	int numeroMaior;
	
	public int maiorNumero(int numero1, int numero2) {
		
		if(numero1 > numero2) {
			numeroMaior = numero1;
		} else {
			numeroMaior = numero2;
		}
		
		return numeroMaior;
	}
}
