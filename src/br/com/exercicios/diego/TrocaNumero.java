package br.com.exercicios.diego;

/*
 * Crie uma classe java TrocaNumero que contenha um m�todo que receba dois
 * n�meros NumA e NumB, nessa ordem, e imprima em ordem inversa, isto �, se os
 * dados lidos forem NumA = 5 e NumB = 9, por exemplo, devem ser impressos na
 * ordem NumA = 9 e NumB = 5.
 */

public class TrocaNumero {
	
	public void trocarNumero(int numA, int numB) {
		System.out.print(numB + "" + numA);
	}
	
}
