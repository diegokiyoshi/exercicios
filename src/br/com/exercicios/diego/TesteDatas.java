package br.com.exercicios.diego;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TesteDatas {

	public static void main(String[] args) throws ParseException {
		Date dataAgora = new Date();
		System.out.println("Data de hoje: " + dataAgora + "\n");

		SimpleDateFormat dataComBarrasFormat = new SimpleDateFormat(
				"dd/MM/yyyy hh:mm:ss"); /*
										 * Devemos passar o formato de data a ser exibido, isso qdo utilizamos o format
										 */
		String dataAgoraTeste = dataComBarrasFormat
				.format(dataAgora); /*
									 * Utilizando o m�todo format da classe SimpleDateFormat, receber� um objeto do
									 * tipo Date, e o objeto ser� convertido em String
									 */
		System.out.println("Data de hoje utilizando SimpleDateFormat: " + dataAgoraTeste + "\n");

		SimpleDateFormat dataComBarrasParse = new SimpleDateFormat();
		// System.out.println(dataComBarrasParse.parse(dataAgoraTeste));

		String dataParseParaFormat = dataComBarrasFormat
				.format(dataComBarrasParse.parse(dataAgoraTeste)); /*
																	 * Transformando um objeto Date (utilizando o m�todo
																	 * parse para pegar o Date) em formato String
																	 */
		System.out.println("Convertendo Parse para Format: " + dataParseParaFormat);

		Date dataFormatParaParse = dataComBarrasParse.parse(
				dataComBarrasFormat.format(dataAgora)); /*
														 * Transformando um objeto de data String (utilizando o m�todo
														 * format para pegar a String de data) para o formato Date
														 */
		System.out.println("Convertendo Format para Parse: " + dataFormatParaParse);

		Calendar dataHoje = Calendar.getInstance();
		dataHoje.setTime(dataAgora); /*
										 * O m�todo setTime() da classe Calendar, recebe como par�metro um objeto Date
										 */
		System.out.println(dataHoje.getTime());

		dataHoje.add(Calendar.DAY_OF_MONTH, 2);
		dataHoje.add(Calendar.MONTH, 2);
		
		SimpleDateFormat dataCalendarFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss:sss");
		String dataCalendarParaString = dataCalendarFormat.format(dataHoje.getTime());
		System.out.println("@@@@@ " + dataCalendarParaString + "\n");
		
		String teste = dataCalendarFormat.format(dataCalendarFormat.parse(dataCalendarFormat.format(dataHoje.getTime())));
		
		System.out.println(teste);
		
		System.out.println(dataHoje.getTime());
		
		
	}
}
