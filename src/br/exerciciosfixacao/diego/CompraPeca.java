
/*
  4. Escrever um algoritmo que l�:
- a porcentagem do IPI a ser acrescido no valor das pe�as
- o c�digo da pe�a 1, valor unit�rio da pe�a 1, quantidade de pe�as 1
- o c�digo da pe�a 2, valor unit�rio da pe�a 2, quantidade de pe�as 2
O algoritmo deve calcular o valor total a ser pago e apresentar o resultado.
F�rmula : (valor1*quant1 + valor2*quant2)*(IPI/100 + 1)

*/

package br.exerciciosfixacao.diego;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompraPeca {

	public static void main(String[] args) {
		
		Double valorTotal = 0.0;
		Integer quantidadePeca = 0;
		Double ipiDoValorTotal = 0.0;
		
		List<Peca> pecas = adicionarPeca();
		
		
	}
	
	private static List<Peca> adicionarPeca() {
		List<Peca> listaPeca = new ArrayList<Peca>();
		Scanner scan = new Scanner(System.in);
		String opcaoAdicionaOuFecha;
		Boolean desejaAdicionarPeca = Boolean.TRUE;
		
		while(desejaAdicionarPeca) {
			
			System.out.println("Digite a pe�a a ser adicionada na Nota Fiscal ou "
					+ "digite 'FECHAR' para fechar o pedido e exibir os dados e fechar a Nota Fiscal");
			opcaoAdicionaOuFecha = scan.next();
			
			if(!opcaoAdicionaOuFecha.equalsIgnoreCase("FECHAR")) {
				
				Peca peca = new Peca();
				
				System.out.println("Digite o nome da pe�a");
				String nomePeca = scan.next();
				peca.setNome(nomePeca);
				
				System.out.println("Digite o c�digo da pe�a");
				Integer codigoPeca = scan.nextInt();
				peca.setCodigo(codigoPeca);
				
				System.out.println("Digite o pre�o da pe�a");
				Double precoPeca = scan.nextDouble();
				peca.setPreco(precoPeca);
				
				listaPeca.add(peca);
			}
		}
		
		return listaPeca;
			
		
	}

}
