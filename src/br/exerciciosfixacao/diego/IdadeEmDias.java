/*
  1. Fa�a um algoritmo que leia a idade de uma pessoa expressa em anos, meses e dias e
mostre-a expressa em dias. Leve em considera��o o ano com 365 dias e o m�s com 30.
(Ex: 3 anos, 2 meses e 15 dias = 1170 dias.)

*/

package br.exerciciosfixacao.diego;

import java.util.Scanner;

public class IdadeEmDias {
	
	public static void main(String[] args) {
	
		Integer dias = 0;
		Integer anosPessoa = 0;
		Integer mesesPessoa = 0;
		Integer diasPessoa = 0;
		
		IdadeEmDias main = new IdadeEmDias();
		Scanner scan = new Scanner(System.in);
		
//		System.out.println("Digite a idade da pessoa: ");
//		anosPessoa = scan.nextInt();
//		
//		System.out.println("Digite os meses do �ltimo anivers�rio: ");
//		mesesPessoa = scan.nextInt();
//		
//		System.out.println("Digite os dias: ");
//		diasPessoa = scan.nextInt();
//		
//		dias = (365 * anosPessoa) + (30 * mesesPessoa) + (diasPessoa);
//		System.out.println(dias);
		
		anosPessoa = scan.nextInt();
		mesesPessoa = scan.nextInt();
		diasPessoa = scan.nextInt();
		
		System.out.println(main.idadeEmDias(anosPessoa, mesesPessoa, diasPessoa));
		
	}
	
	public Integer idadeEmDias(Integer anosPessoa, Integer mesesPessoa, Integer diasPessoa) {
		
		Integer dias = 0;
		
		dias = (365 * anosPessoa) + (30 * mesesPessoa) + (diasPessoa);
		
		return dias;
	}
	
}
