/*
  2. Fazer um programa que imprima a m�dia aritm�tica dos n�meros 8,9 e 7. A m�dia dos
n�meros 4, 5 e 6. A soma das duas m�dias. A m�dia das m�dias.

*/

package br.exerciciosfixacao.diego;

import java.util.Scanner;

public class Media {
	public static void main(String[] args) {
	
		Media main = new Media();
		Scanner scan = new Scanner(System.in);
		
		Double num1 = scan.nextDouble();
		Double num2 = scan.nextDouble();
		Double num3 = scan.nextDouble();
		Double num4 = scan.nextDouble();
		Double num5 = scan.nextDouble();
		Double num6 = scan.nextDouble();
		
		Double media1;
		Double media2;
		
		media1 = main.calcularMediaAritmetica(num1, num2, num3);
		media2 = main.calcularMediaAritmetica(num4, num5, num6);
		main.somarMedia(media1, media2);
		main.calcularMediasDasMedias(media1, media2);
	}
	
	public Double calcularMediaAritmetica(Double numero1, Double numero2, Double numero3) {
		
		Double media = (numero1 + numero2 + numero3)/3;
		return media;
	}
	
	public Double somarMedia(Double media1, Double media2) {
		
		Double somaMedias = media1 + media2;
		return somaMedias;
	}
	
	public Double calcularMediasDasMedias(Double media1, Double media2) {
		
		Double mediaDasMedias;
		mediaDasMedias = somarMedia(media1, media2)/2;
		return mediaDasMedias;
	}
}
