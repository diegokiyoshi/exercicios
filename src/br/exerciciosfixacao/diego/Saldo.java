/*
  3. Informar um saldo e imprimir o saldo com reajuste de 1%.
  */

package br.exerciciosfixacao.diego;

import java.util.Scanner;

public class Saldo {
	
	public static void main(String[] args) {
		
		Double saldo;
		Scanner scan = new Scanner(System.in);
		
		Saldo main = new Saldo();
		saldo = scan.nextDouble();
		
		main.imprimirSaldo(saldo);
	}
	
	public void imprimirSaldo(Double saldo) {
		
		System.out.println(saldo * 1.01);
	}

}
